NOTES:
- how to update state and/or context if service POST is complete in another part of the app. claim an offer, then propogate that new response data through the app
- each service can "register" custom event listeners that will be looped over and called on the completion of specific request methods. Just like `Listener` service.
- how to store already fetched state? where is it provided to context? this is a "persistence" layer
  - sessionStorage?
  - read in from sessionStorage in each service file? in the final file?
- how to separate out unique data fetches from the defaults
- how to send the response of a promise that relies on the response of another promise, so it's not `Promise.all`, but rather intentionally chained promises
- ALL POST responses should return the same data as their GET counterparts
  - OR the response is so precise that it can easily overwrite that specific part of the full data object without much or any parsing
- Where to put media queries
  - A) immediately nested inside the thing they modify
  - B) all together with children/elements nested inside them



Write up GlobalListeners + Listener + withWindowDimensions
- Listener service is for cross-component communication and is shared across the whole app; `import` as many times as needed
- GlobalListeners is a way to do a single `window.addEventListener` and then dispatches/triggers corresponding `GLOBAL_[EVENT_NAME]` events
  - use this instead of window.addEventListener
- withWindowDimensions HOC changes on resize and provides window size values and content container size values