# Directory Structure

This repo is a supplement for the RJR Architecture presentation and is a general guideline for how to structure a project.

## Documentation

- [Architecture](/documentation/architecture.md)
- [Naming Conventions](/documentation/naming.md)

### The Things

- [Services](/app/services/README.md)
- [Routes](/app/routes/README.md)
