# Architecture - Documentation

Users hit `routes`, which call to render `views` comprised of `components`, `styles`, and `utilities` that rely on the data from `context` providers populated via [`services`](/app/services/README.md).

## Organization

As a general rule, prefer a flat structure where possible. `index` files can serve as either the entirety of the code you wish to access OR a Table of Contents-like file that imports other, more modular, files. Generally, modularity and code separation is needed if you find similar code, files, etc... whose quantity is >= 3.

### Contrived Examples:

**Complexity - Level I**
```
📁partials/
  Hero.js
```
  
**Complexity - Level II**
```
📁partials/
  📁Hero/
    styles.js
    index.js
```

**Complexity - Level III**
```
📁partials/
  📁Hero/
    📁styles/
      README.md
      index.js
    index.js
    tests.js
    README.md
```


## Directory Structure

### Top-level directories

Name | Description
--- | ---
`app/` | All **client-side** code compiled to create the site/application
`server/` | All **server-side** code used to serve up the site/application
`public/` | All non-compiled assets used for the site AND the build/compile destination for source assets from `app/`. (Examples: images, favicon, etc...)
`documentation/` | Site or application-specific documentation. *Prefer Dropbox for client assets and Google Drive for internal, shared documents.* (Examples: THIS document, setup process, site/application-specific code nuances).
`webpack/` | All `webpack`-specific configurations to maintain modularity.
