import styled from 'styled-components';

export const Container = styled.section`
  margin: 0;
  padding: 1rem;
  background-color: #333;
  color: white;
`;

export default {
  Container,
}