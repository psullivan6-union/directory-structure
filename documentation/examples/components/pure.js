import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

// Styles
import { Container } from './styles';


class ForgeComponent extends PureComponent {
  componentDidMount() {
    console.log('ForgeComponent - componentDidMount', 'go GET the data');
  }

  render() {
    const { TEST } = this.props;

    return (
      <Container>
        <h1>ForgeComponent</h1>
        <h2>{ `TEST prop value: ${TEST}` }</h2>
      </Container>
    )
  }
}

ForgeComponent.defaultProps = {
  TEST: 'default value',
};

ForgeComponent.propTypes = {
  TEST: PropTypes.string,
};

export default ForgeComponent;