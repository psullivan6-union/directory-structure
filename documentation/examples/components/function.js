import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Styles
import { Container } from './styles';


const ForgeComponent = ({ TEST }) => (
  <Container>
    <h1>ForgeComponent</h1>
    <h2>{ `TEST prop value: ${TEST}` }</h2>
  </Container>
)

ForgeComponent.defaultProps = {
  TEST: 'default value',
};

ForgeComponent.propTypes = {
  TEST: PropTypes.string,
};

export default ForgeComponent;