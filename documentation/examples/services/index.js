import Fetcher from 'Utilities/Fetcher';
import BugSnag from 'Utilities/BugSnag';

// =============================================================================
// DEFAULT
// =============================================================================
export const exampleGet = ({ defaultResponse = [] }) => (
  Fetcher.get('/api/GET-endpoint-example')
    .then(data => data)
    .catch(error => {
      BugSnag(error);

      // since this request is non-critical, send the default response down-stream
      return defaultResponse;
    })
);

// =============================================================================
// CRITICAL
// =============================================================================
export const criticalGet = ({ defaultResponse = [] }) => (
  Fetcher.get('/api/GET-endpoint-example')
    .then(data => data)
    .catch(error => {
      // ensure the error is properly logged for investigation
      BugSnag(error);

      // since this is critical, ensure the error is thrown so the down-stream Promise also fails
      throw error;
    })
);

export default {
  exampleGet,
}
