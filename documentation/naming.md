# Naming - Documentation

## Asset Types - Naming Conventions

**Directory Convention**: the convention for the directory name  
**Filename Convention**: the convention for the filename  
**Code Convention**: the convention for the variable, etc... in the actual code  

Type | Directory Convention | Filename Convention | Code Convention
--- | --- | --- | ---
`routes` | 🌭kebab-case | 🌭kebab-case | 🐫UpperCamelCase
`views` | 🌭kebab-case | 🐫UpperCamelCase | 🐫UpperCamelCase
`components` | 🐫UpperCamelCase | 🐫UpperCamelCase | 🐫UpperCamelCase
`styles` | 🐫UpperCamelCase | 🐫UpperCamelCase | 🐫UpperCamelCase
`utilities` | 👇lowerCamelCase | 👇lowerCamelCase | 👇lowerCamelCase
`context` | 🐫UpperCamelCase | 🐫UpperCamelCase | 🐫UpperCamelCase
`services` | 🐫UpperCamelCase | 🐫UpperCamelCase | 🐫UpperCamelCase