import styled from 'styled-components';
import Header from './Header';
import Container from './Container';
import Footer from './Footer';

const heightImage = slopeCalc({
  'height': { sm: 350, xl: 850 },
  'padding-bottom': { sm: 120 },
  'padding-top': { sm: 0 }
});
const heightNormal = slopeCalc({
  'height': { sm: 200, xl: 625 }
});

const ForgeStyledComponent = styled.section`
  background-color: ${({ theme }) => theme.background};
  color: ${({ theme }) => theme.foreground};
  ${sectionSpacing}
  ${({ image }) => image === true ? heightImage : heightNormal};
  position: relative;

  ${media.sidebar`
    .nav-open & {
      height: auto;
    }
  `}
`;

HeroArticle.displayName = 'StyledHeroArticle';

HeroArticle.Action = Action;
HeroArticle.Container = Container;
HeroArticle.Header = Header;

export default HeroArticle;
