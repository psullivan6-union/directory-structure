




// [TODO] REPLACE WITH NEWEST, WORKING FILE





import React, { Component, Fragment } from 'react';

// Services
import Service1 from 'Services/Service1';
import Service2 from 'Services/Service2';

// Components
import Loader from 'Components/Loader';

// Context
import { PageProvider } from 'Context/PageContext';
import { ThemeProvider } from 'styled-components';

// Styles
import GlobalStyle from 'Styles/global';


class Page extends Component {
  state = {
    loading: true,
  }

  componentDidMount() {
    // Default data promises
    const dataPromise1 = Service1.fetchData();
    const dataPromise2 = Service1.fetchData();

    // Custom additional data promises
    const { additionalData } = this.props;

    const promises = additionalData.map(item => item.promise());
    const promiseNames = additionalData.map(item => item.name);

    Promise.all(
      [
        dataPromise1,
        dataPromise2,
        ...promises,
      ]
    )
      .then((
        [
          userData,
          offers,
          warnings,
          ...additionalDataResponse
        ]
      ) => {
        const state = {
          loading: false,
          userData,
          offers,
          warnings,
        };

        additionalDataResponse.forEach((item, index) => {
          state[promiseNames[index]] = item;
        });

        this.setState(state);
      })
      .catch((error) => {
        console.log('ERROR', error);
      });
  }

  render() {
    const { loading } = this.state;
    const { children } = this.props;

    return (
      <Fragment>
        <GlobalStyle />
        <PageProvider
          value={ { ...this.state } }
        >
          <ThemeProvider theme={ theme }>
            {
              (loading)
                ? <Loader />
                : (
                  <Fragment>
                    <NavBar />
                    { children }
                  </Fragment>
                )
            }
          </ThemeProvider>
        </PageProvider>
      </Fragment>
    );
  }
}

Page.propTypes = {
  additionalData: PropTypes.arrayOf(PropTypes.shape({
    name    : PropTypes.string,
    promise : PropTypes.func,
  })),
};

Page.defaultProps = {
  additionalData: [],
};

export default Page;
