# Services

Services are utility-like files used to abstract the API interaction layer. They are used to dynamically populate the site/application with data.

## Assumptions

It should always be assumed that a failure can happen when requesting data, so it is up to the developer to determine whether to send a default data response or display the error state (via redirect, component, etc...).

- all services are Promise-based
- all services catch errors within the service file (regardless of criticality)
- all services leverage an error reporting utility
- critical services should always throw any error
- non-critical services should return a `defaultResponse` object if the service fails

## Layers of Abstraction

1. `axios` is an abstraction on-top of native `fetch`  
2. `Fetcher` (or similar) utility is an abstraction of `axios` with `csrf`, `json`, and associated defaults pre-defined  
3. `UserService` service uses the `Fetcher` (or similar) utility and makes calls to the hard-coded URLs  
4. `Component` file calls the `UserService` method
