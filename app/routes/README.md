# Routes

## Naming Convention

| Type | Convention | Examples |
|---|---|---|
| File | `kebab-case` | `example-route-group.js` <br /> `products.js` <br /> `index.js` |
| Directory | `kebab-case` | **`our-spirit/`**`sub-group.js` <br /> **`our-spirit/`**`sub-group-two.js` <br /> **`our-spirit/`**`index.js` |


Each file shall be `kebab-case` named in lowercase and represent a grouping of routes. These route groups should parallel in the `views` directory with a directory of the same name. For example:

`/app/routes/example-route-group.js`
`/app/views/ExampleGroup/SpecialViewOne/`
`/app/views/ExampleGroup/SpecialViewTwo/`
